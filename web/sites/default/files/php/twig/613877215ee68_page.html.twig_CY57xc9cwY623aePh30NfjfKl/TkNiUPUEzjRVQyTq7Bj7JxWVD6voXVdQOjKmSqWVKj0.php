<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/Ineff/templates/system/page.html.twig */
class __TwigTemplate_cc402d53472c2be25ea07d6520b75f57c13f51c2a8e3b8a93ac0d81f3516e112 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<header aria-label=\"Site header\" class=\"header\" id=\"header\" role=\"banner\">
  <div class=\"loyout-wrapper\">
";
        // line 4
        echo "    ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "header", [], "any", false, false, true, 4), 4, $this->source), "html", null, true);
        echo "
    <div class=\"loyout-wrapper__links\">
      <ul>
        <li><a href=\"#\" class=\"active\">Главная</a></li>
        <li><a href=\"#\">Новости</a></li>
        <li><a href=\"#\">О компании</a></li>
        <li><a href=\"#\">Кейсы</a></li>
        <li><a href=\"#\">Контакты</a></li>
      </ul>
    </div>
    <div class=\"loyout-wrapper__contacts\">
      <ul>
        <li>
          <a href=\"tel:+78126029199\">+7 812 602 91 99</a>
        </li>
        <li>
          <a href=\"mailto:info@ineff.ru\">info@ineff.ru</a>
        </li>
      </ul>
    </div>
    <div class=\"burger\">
      <div class=\"burger-item\"></div>
    </div>
  </div>

  <div class=\"navigation-overlay\">
    <ul>
      <li><a href=\"#\" class=\"active\">Главная</a></li>
      <li><a href=\"#\">Новости</a></li>
      <li><a href=\"#\">О компании</a></li>
      <li><a href=\"#\">Кейсы</a></li>
      <li><a href=\"#\">Контакты</a></li>
    </ul>
  </div>


</header>

  <main aria-label=\"Site main content\" class=\"content\" id=\"content\" role=\"main\">
    ";
        // line 43
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "content", [], "any", false, false, true, 43), 43, $this->source), "html", null, true);
        echo "
  </main>

<footer aria-label=\"Site footer\" class=\"footer\" id=\"footer\" role=\"contentinfo\">
  <div class=\"loyout-wrapper\">
    <div class=\"footer--top\">
      ";
        // line 49
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_logo", [], "any", false, false, true, 49), 49, $this->source), "html", null, true);
        echo "
      ";
        // line 50
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_navigation", [], "any", false, false, true, 50), 50, $this->source), "html", null, true);
        echo "
      ";
        // line 51
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_contacts", [], "any", false, false, true, 51), 51, $this->source), "html", null, true);
        echo "
      ";
        // line 52
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_products", [], "any", false, false, true, 52), 52, $this->source), "html", null, true);
        echo "

      <div class=\"footer-top__anchor\">
        <a href=\"#\"></a>
      </div>
    </div>
  </div>
  <div class=\"footer--bottom\">
          ";
        // line 60
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_copyright", [], "any", false, false, true, 60), 60, $this->source), "html", null, true);
        echo "
";
        // line 62
        echo "    <div class=\"loyout-wrapper\">
";
        // line 65
        echo "    </div>
  </div>
</footer>
";
    }

    public function getTemplateName()
    {
        return "themes/Ineff/templates/system/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 65,  122 => 62,  118 => 60,  107 => 52,  103 => 51,  99 => 50,  95 => 49,  86 => 43,  43 => 4,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<header aria-label=\"Site header\" class=\"header\" id=\"header\" role=\"banner\">
  <div class=\"loyout-wrapper\">
{#    {{ page.branding }}#}
    {{ page.header }}
    <div class=\"loyout-wrapper__links\">
      <ul>
        <li><a href=\"#\" class=\"active\">Главная</a></li>
        <li><a href=\"#\">Новости</a></li>
        <li><a href=\"#\">О компании</a></li>
        <li><a href=\"#\">Кейсы</a></li>
        <li><a href=\"#\">Контакты</a></li>
      </ul>
    </div>
    <div class=\"loyout-wrapper__contacts\">
      <ul>
        <li>
          <a href=\"tel:+78126029199\">+7 812 602 91 99</a>
        </li>
        <li>
          <a href=\"mailto:info@ineff.ru\">info@ineff.ru</a>
        </li>
      </ul>
    </div>
    <div class=\"burger\">
      <div class=\"burger-item\"></div>
    </div>
  </div>

  <div class=\"navigation-overlay\">
    <ul>
      <li><a href=\"#\" class=\"active\">Главная</a></li>
      <li><a href=\"#\">Новости</a></li>
      <li><a href=\"#\">О компании</a></li>
      <li><a href=\"#\">Кейсы</a></li>
      <li><a href=\"#\">Контакты</a></li>
    </ul>
  </div>


</header>

  <main aria-label=\"Site main content\" class=\"content\" id=\"content\" role=\"main\">
    {{ page.content }}
  </main>

<footer aria-label=\"Site footer\" class=\"footer\" id=\"footer\" role=\"contentinfo\">
  <div class=\"loyout-wrapper\">
    <div class=\"footer--top\">
      {{ page.footer_logo }}
      {{ page.footer_navigation }}
      {{ page.footer_contacts }}
      {{ page.footer_products }}

      <div class=\"footer-top__anchor\">
        <a href=\"#\"></a>
      </div>
    </div>
  </div>
  <div class=\"footer--bottom\">
          {{ page.footer_copyright }}
{#          {{ page.copyright }}#}
    <div class=\"loyout-wrapper\">
{#      <p>Политика конфиденциальности</p>#}
{#      <p>Copyright © 2015-2021 ООО \"ИНЭФФ\". Все права защищены.</p>#}
    </div>
  </div>
</footer>
", "themes/Ineff/templates/system/page.html.twig", "/var/www/html/web/themes/Ineff/templates/system/page.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array();
        static $filters = array("escape" => 4);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}


(function ($, Drupal) {
  'use strict';

  /** menu overlay **/
  Drupal.behaviors.menu = {
    attach: function (context, settings) {

      $('.burger', context).once().on('click', function () {
        $(this).toggleClass("active-menu");
        $('.navigation-overlay').toggleClass("active-overlay");
      });

    }
  }


  /** Tabs for report form **/
  Drupal.behaviors.tabs = {
    attach: function (context, settings) {

      $(function() {
        // Active the first thumb & panel
        $(".field--name-field-report-tab .field__item:first-child").addClass("active").closest(".paragraph--type--report-item").find(".field--name-field-report-table .field__item:first-child").show();

        $(".field--name-field-report-tab .field__item").click(function() {
          // Cancel the siblings'
          $(this).siblings(".field--name-field-report-tab .field__item").removeClass("active").closest(".paragraph--type--report-item").find(".field--name-field-report-table .field__item").hide();
          // Active the thumb & panel
          $(this).addClass("active").closest(".paragraph--type--report-item").find(".field--name-field-report-table .field__item").eq($(this).index(".field--name-field-report-tab .field__item")).show();
        });
      });
    }
  }

  /** grid masonry **/
  Drupal.behaviors.masonry = {
    attach: function (context, settings) {

      // $('.grid').masonry({
      //   // options...
      //   itemSelector: '.grid-item',
      //   columnWidth: 200,
      //   gutter: 1
      // });


      let magicGrid = new MagicGrid({
        container: '.grid',
        animate: true,
        gutter: 20,
        static: true,
        useMin: true
      });

      magicGrid.listen();



    }
  }



  /** Slider for trusted section **/
  Drupal.behaviors.slider = {
    attach: function (context, settings) {

      $(document).ready(function(){
        $('.field--name-field-slide').slick({
          slidesToShow: 4,
          slidesToScroll: 1,
          autoplay: true,
          autoplaySpeed: 2000,
          centerPadding: '30px',
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 3,
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 568,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 375,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
        });
      });

    }
  }





  //
  // /** Accordion **/
  // Drupal.behaviors.accordion = {
  //   attach: function (context, settings) {
  //
  //     $('.paragraph--type--answer-item', context).once().on('click', function () {
  //       $(this).toggleClass("act ive-accordeon");
  //     });
  //
  //   }
  // }
  //


})(jQuery, Drupal);
